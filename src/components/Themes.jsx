import React from "react";

const Themes = () => {
  return (
    <section className="services-section ptb-70 gray-light-bg" id="challenges">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="section-heading text-center mb-5">
              <strong className="color-secondary">
              NU x EXTOSOFT Hacks 1.0 Themes
              </strong>
              <h2> Themes</h2>
              <span className="animate-border mr-auto ml-auto mb-4"></span>
              <p>
              Feel free to hack on whatever you'd like or make it art and craft themed for the fun of it. These events are open to all skill levels, from beginners to the most veteran developers. While we'd love to have you build something on the theme, our themes are only here for inspiration. Please build whatever you'd like and focus on having fun!
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="services-single text-center p-5 my-md-3 my-lg-3 my-sm-0 shadow-sm white-bg rounded">
              <span className="icon-lg color-secondary d-block mb-4">
                <i className="fa fa-television" aria-hidden="true"></i>
              </span>
              <h5>Performance Test</h5>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="services-single text-center p-5 my-md-3 my-lg-3 my-sm-0 shadow-sm white-bg rounded">
              <span className="icon-lg color-secondary d-block mb-4">
                <i className="fas fa-microchip" aria-hidden="true"></i>
              </span>
              <h5>Technology</h5>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="services-single text-center p-5 my-md-3 my-lg-3 my-sm-0 shadow-sm white-bg rounded">
              <span className="icon-lg color-secondary d-block mb-4">
                <i className="fa fa-cogs" aria-hidden="true"></i>
              </span>
              <h5>Automation</h5>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="services-single text-center p-5 my-md-3 my-lg-3 my-sm-0 shadow-sm white-bg rounded">
              <span className="icon-lg color-secondary d-block mb-4">
                <i className="fa fa-cloud" aria-hidden="true"></i>
              </span>
              <h5>Cloud</h5>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="services-single text-center p-5 my-md-3 my-lg-3 my-sm-0 shadow-sm white-bg rounded">
              <span className="icon-lg color-secondary d-block mb-4">
                <i className="fa fa-robot" aria-hidden="true"></i>
              </span>
              <h5> AI/ML </h5>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="services-single text-center p-5 my-md-3 my-lg-3 my-sm-0 shadow-sm white-bg rounded">
              <span className="icon-lg color-secondary d-block mb-4">
                <i className="fas fa-brain" aria-hidden="true"></i>
              </span>
              <h5>Open Innovation</h5>
            </div>
          </div>
          
        </div>
      </div>
    </section>
  );
};

export default Themes;
