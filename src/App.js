import About from "./components/About";
import FAQS from "./components/FAQS";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Hero from "./components/Hero";
import Loader from "./components/Loader";
import ParticipationCertification from "./components/ParticipationCertification";
import Prizes from "./components/Prizes";
import Schedule from "./components/Schedule";
import Sponsers from "./components/Sponsers";
import Team from "./components/Team";
import Themes from "./components/Themes";
import Timeline from "./components/timeline/Timeline";
function App() {
  return (
    <div className="App">
      <Loader />
      <Header />
      <div className="main">
        <Hero />
        <About />
        <Timeline/>
        <Themes />
        <Schedule />
        <Prizes />
        <ParticipationCertification />
        <Sponsers />
        <Team />
        <FAQS />
      </div>
      <Footer />
    </div>
  );
}

export default App;
